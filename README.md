# Natural Language Processing mit Python

This repository contains the notebooks and handout for the following presentation:

title
: Natural Language Processing mit Python

author
: Thomas Timmermann

date
: March 31, 2021

occasion
: Heise Academy workshop


## Access via Binder

The notebooks can be accessed at

https://mybinder.org/v2/gl/thomtimm%2Fheise-nlp-20210331-notebooks/master

but beware mybinder's memory limit of 2GB.


## Local Usage

Install dependencies:

``` sh
pip install -r requirements.txt
```

Start jupyter:

``` sh
jupyter lab notebooks
```
