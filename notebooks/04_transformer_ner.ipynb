{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Named Entity Recognition mit Transformern"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir verwenden nochmal den Datensatz\n",
    "\n",
    "> [A Dataset of German Legal Documents for Named Entity Recognition](https://github.com/elenanereiss/Legal-Entity-Recognition)\n",
    " \n",
    "aus der Publikation\n",
    "\n",
    "> Leitner, E., Rehm, G., Moreno-Schneider, J.  \n",
    "> **_Fine-grained Named Entity Recognition in Legal Documents._**  \n",
    "> In Proceedings of the 15th International Conference SEMANTiCS2019.\n",
    "\n",
    "und verwenden nun die Python-Bibliothek [huggingface/transformers](https://huggingface.co/transformers), um einen Transformer auf das Annotieren der juristischen Texte zu trainieren."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schritt 1: Daten einlesen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Erinnern wir uns an das Format der Daten:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 20 data/legal/01_raw/bgh.conll"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir lesen solche Dateien als Listen von Sätzen ein, wobei jeder Satz eine Liste von Wort-Annotations-Paaren ist:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "MAX_LEN = 50\n",
    "\n",
    "def load_data(filename: str):\n",
    "    samples = []\n",
    "    with open(filename, 'r') as file:\n",
    "        sentence = []\n",
    "        for line in file:\n",
    "            line = line[:-1] # trim newline\n",
    "            if line:\n",
    "                token, full_tag = line.split()\n",
    "                tag = full_tag.split('-')[-1]\n",
    "                sentence.append((token, tag))\n",
    "            else:\n",
    "                samples.append(sentence[:MAX_LEN])\n",
    "                sentence = []\n",
    "    return samples\n",
    "\n",
    "TRAIN_FILE = 'data/legal/01_raw/bag.conll'\n",
    "train_samples = load_data(TRAIN_FILE)\n",
    "\n",
    "VAL_FILE = 'data/legal/01_raw/bgh.conll'\n",
    "val_samples = load_data(VAL_FILE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir betrachten den ersten Trainings-Satz, und davon die ersten 10 Wort-Annotations-Paare:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "train_samples[0][:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Werfen wir einen Blick auf die Anzahl an Trainings- und Validierungs-Sätzen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "len(train_samples), len(val_samples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Menge aller Annotationen bestimmen wir wie folgt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def get_schema(samples):\n",
    "    return ['_'] + sorted({tag for sentence in samples for _, tag in sentence})\n",
    "\n",
    "schema = get_schema(train_samples + val_samples)\n",
    "print(' '.join(schema))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schritt 2: Transformer-Modell laden"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Bibliothek [huggingface/transformers](https://huggingface.co/transformers) bietet viele\n",
    "\n",
    "- vortrainierte State-of-the-Art-Transformer-Modelle wie BERT, GPT-2 und viele aktuellere,\n",
    "- Wrapper für das Fine-Tuning auf Zielaufgaben wie Satz-Klassifikation, Token-Klassifikation, Summarization und so weiter\n",
    "- Glue-Code zum Trainieren der Modelle.\n",
    "\n",
    "Wir benutzen im Folgenden\n",
    "\n",
    "- einen auf die deutsche Sprache vortrainierten BERT,\n",
    "- verpackt für Token-Klassifikation\n",
    "- in Form eines TensorFlow-Modells.\n",
    "\n",
    "Wie die meisten Transformer-Modelle ist BERT recht groß, das Laden dauert entsprechend lang.\n",
    "\n",
    "Und hier kommt das **gotcha**: Die Transfomer-Modelle sind allesamt mit Subword-Level-Tokenisierern vortrainiert, und für jedes Modell müssen wir ebenfalls den zugehörigen Tokenisierer verwenden!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from transformers import AutoConfig, TFAutoModelForTokenClassification, AutoTokenizer\n",
    "import tensorflow as tf\n",
    "\n",
    "MODEL_NAME = 'bert-base-german-cased' # another option would be 'german-nlp-group/electra-base-german-uncased' \n",
    "\n",
    "def build_model(metrics='accuracy'):\n",
    "    tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)\n",
    "    config = AutoConfig.from_pretrained(MODEL_NAME, num_labels=len(schema))\n",
    "    model = TFAutoModelForTokenClassification.from_pretrained(MODEL_NAME, config=config)\n",
    "    optimizer = tf.keras.optimizers.Adam(lr=0.00001)\n",
    "    loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)\n",
    "    model.compile(optimizer=optimizer, loss=loss, metrics=metrics)\n",
    "    return tokenizer, model\n",
    "\n",
    "tokenizer, model = build_model()\n",
    "model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schritt 3: Vorverarbeitung"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Der Tokenisierer von BERT behandelt die gebräuchlichsten Wörter als ein Token, zerlegt aber seltenere Wörter in mehrere Teilwort-Token.\n",
    "Dadurch vermeidet man das Out-of-Vocabulary-Problem:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "for utterance in ['Das', 'Das ist', 'Das ist eine hoch interessante Frage', 'Das ist eine hochinteressante Frage']:\n",
    "    print(tokenizer(utterance))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Für uns bedeutet das aber einen Mehraufwand: wir müssen die Annotationen entsprechend auf Teilwörter übertragen und nutzen dafür folgende Funktion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def tokenize_sample(sample):\n",
    "    seq = [(subtoken, tag) for token, tag in sample for subtoken in tokenizer(token)['input_ids'][1:-1]]\n",
    "    return [(3, 'O')] + seq + [(4, 'O')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Länge der Sätze erhöht sich dadurch meist:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "print(len(train_samples[1]))\n",
    "print(len(tokenize_sample(train_samples[1])))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun wenden wir diese Vorverarbeitung auf alle Beispiel-Sätze an und ersetzen die Annotationen durch ihren jeweiligen Index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from tqdm import tqdm\n",
    "\n",
    "def preprocess(samples):\n",
    "    tag_index = {tag: i for i, tag in enumerate(schema)}\n",
    "    tokenized_samples = list(tqdm(map(tokenize_sample, samples)))\n",
    "    max_len = max(map(len, tokenized_samples))\n",
    "    X = np.zeros((len(samples), max_len), dtype=np.int32)\n",
    "    y = np.zeros((len(samples), max_len), dtype=np.int32)\n",
    "    for i, sentence in enumerate(tokenized_samples):\n",
    "        for j, (subtoken_id, tag) in enumerate(sentence):\n",
    "            X[i, j] = subtoken_id\n",
    "            y[i,j] = tag_index[tag]\n",
    "    return X, y\n",
    "\n",
    "X_train, y_train = preprocess(train_samples)\n",
    "X_val, y_val = preprocess(val_samples)\n",
    "X_train.shape, X_val.shape, y_train.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schritt 4: Trainieren des Modells"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Bevor wir das Training starten, das recht langsam laufen wird, bauen wir uns eine Metrik, welche die Accuracy pro Annotations-Klasse ausgibt. Den Code kann man getrost überspringen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "def build_tag_accuracy(index, name):\n",
    "    def tag_accuracy(y_true, y_pred):\n",
    "        y_pred_index = tf.math.argmax(y_pred, axis=-1)\n",
    "        y_t_b = y_true == index\n",
    "        y_p_b = y_pred_index == index\n",
    "        return tf.reduce_mean(tf.where(y_t_b == y_p_b, 1.0, 0.0))\n",
    "\n",
    "    tag_accuracy.__name__ = 'accuracy_' + name\n",
    "    return tag_accuracy\n",
    "\n",
    "metrics = [build_tag_accuracy(index, name) for index, name  in enumerate(schema)]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun nutzen wir eine einfache Trainingsfunktion, die im Wesentlichen die  `fit`-Methode des Modells aufruft:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "def train(model, nr_samples=-1, epochs=10, batch_size=16):\n",
    "    train_data = (tf.constant(X_train[:nr_samples]), tf.constant(y_train[:nr_samples]))\n",
    "    history = model.fit(*train_data, validation_split=0.2, epochs=epochs, batch_size=batch_size)\n",
    "    return model, pd.DataFrame(history.history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Testen wir, ob alles klappt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "_, model = build_model(metrics=metrics + ['accuracy'])\n",
    "model, history = train(model, nr_samples=50, epochs=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Das scheint zu klappen und wird auf allen Daten lange dauern. Deswegen bereiten wir gleich erstmal die Auswertung. Doch zuerst die Visualisierung des Trainingsverlaufs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "sns.set()\n",
    "\n",
    "def plot_history(history):\n",
    "    _, (ax1, ax2) = plt.subplots(1,2, figsize=(15,5))\n",
    "    history[['loss', 'val_loss']].plot.line(ax=ax1)\n",
    "    history[['accuracy', 'val_accuracy']].plot.line(ax=ax2)\n",
    "\n",
    "plot_history(history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schritt 5: Auswertung"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Schauen wir uns an, wie wir das trainierte Modell anwenden können:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "sample_index = 5\n",
    "probs = model.predict(X_val[sample_index: sample_index + 1])[0][0]\n",
    "probs.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Das Modell gibt uns für einen Beispielsatz, der aus 109 Token besteht, eine Matrix mit 109 Zeilen, die jeweils eine Wahrscheinlichkeitsverteilung für die Annotation des Token angeben. Wir haben 21 Annotations-Klassen, also 21 Spalten in der Matrix. Die jeweils vorhergesagte Annotation erhalten wir wie folgt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "preds = np.argmax(probs, axis=-1)\n",
    "np.array(schema)[preds]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun wollen wir von dem Modell eigentlich nicht die Annotation auf Token-Ebene, also pro Teilwort, sondern pro Wort in unserem Ausgangssatz haben!\n",
    "Dazu sammeln wir pro Wort die Annotationen der Teilwörter ein und machen eine Mehrheits-Abstimmung, um die Annotation des Wortes zu bestimmen. \n",
    "Für die Mehrheits-Abstimmung verwenden wir folgende Funktion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def majority_vote(votes):\n",
    "    return np.argmax(np.histogram(votes, bins=len(schema), range=(0, len(schema)))[0])\n",
    "\n",
    "majority_vote(np.array([1,2,1,0,3,1,4]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun können wir Vorhersagen des Modells auf Wort-Ebene zusammenfügen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def align_predictions(sample, predictions):\n",
    "    results = []\n",
    "    i = 1\n",
    "    for token, y_true in sample:\n",
    "        nr_subtoken = len(tokenizer(token)['input_ids']) -2 # beware of start- and end-token\n",
    "        votes = predictions[i:i+nr_subtoken]\n",
    "        i += nr_subtoken\n",
    "        y_pred = schema[majority_vote(votes)]\n",
    "        y_pred_sub = ''.join(schema[p] for p in votes)\n",
    "        results.append((token, y_true, y_pred, y_pred_sub))\n",
    "    return results\n",
    "\n",
    "pd.DataFrame(align_predictions(val_samples[sample_index], preds)[:10], columns= ['Token', 'Truth', 'Prediction', 'Sub-Predictions'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir wenden schließlich das Modell auf unsere Validierungs-Daten wie folgt an:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def predict(model, nr_val_samples=-1):\n",
    "    y_probs = model.predict(X_val[:nr_val_samples])[0]\n",
    "    y_preds = np.argmax(y_probs, axis=-1)\n",
    "    return [align_predictions(sample, predictions) for sample, predictions in zip(val_samples, y_preds)]\n",
    "\n",
    "predictions = predict(model, nr_val_samples=10)\n",
    "predictions[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Schließlich verwenden wir sklearn für die Bewertung der Ergebnisse:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from sklearn.metrics import classification_report\n",
    "\n",
    "def evaluate(predictions):\n",
    "    y_t = [pos[1] for sentence in predictions for pos in sentence]\n",
    "    y_p = [pos[2] for sentence in predictions for pos in sentence]\n",
    "    report = classification_report(y_t, y_p, output_dict=True)\n",
    "    return pd.DataFrame.from_dict(report).transpose().reset_index()\n",
    "\n",
    "scores = evaluate(predictions)\n",
    "print(scores)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Zum Schluss: Überblick behalten mit MLFlow Tracking"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun haben wir alles eingerichtet, um das Modell richtig zu trainieren.\n",
    "Oft verleiten Jupyter-Notebooks dazu, Dinge schnell auszuprobieren, Parameter zu ändern und Experimente durchzurasseln, und wenn man nicht aufpasst, verliert man am Ende nicht nur den Überblick, sondern auch die Zwischenergebnisse und Daten.\n",
    "Eine Lösung für dieses Problem bietet MLFLow Tracking. Am Besten packt man alle Schritte eines Experimentes mit allen Parametern in eine Funktion, die Parameter und Ergebnisse \"loggt\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import mlflow\n",
    "\n",
    "MODEL_DIR = 'data/04_models/large'\n",
    "\n",
    "def run(model_args, train_args, val_args):\n",
    "    mlflow.set_experiment('NER_BERT')\n",
    "    with mlflow.start_run():\n",
    "        mlflow.tensorflow.autolog(every_n_iter=1)\n",
    "        mlflow.log_params(train_args)\n",
    "        mlflow.log_params(val_args)\n",
    "        _, model = build_model(**model_args)\n",
    "        model, history = train(model, **train_args)\n",
    "        model.save(MODEL_DIR, save_format='tf')\n",
    "        mlflow.log_artifacts(MODEL_DIR, 'model')\n",
    "        scores = evaluate(predict(model, **val_args))\n",
    "        scores_dict = {f'{score}_{tag}':value for score, column in scores.set_index('index').to_dict().items() for tag, value in column.items()}\n",
    "        mlflow.log_metrics(scores_dict)\n",
    "        return model, history, scores"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "model, history, score = run(dict(metrics=metrics + ['accuracy']), dict(epochs=2, nr_samples=20), dict(nr_val_samples=20))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun kann man die MLFlow-Oberfläche mit `mlflow ui` starten und erhält einen gewissen Überblick über die durchgeführten Experimente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# !mlflow ui"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nun kann das Rechnen beginnen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "metrics = [build_tag_accuracy(index, name) for index, name  in enumerate(schema)] + ['accuracy']\n",
    "strategy = tf.distribute.MirroredStrategy()\n",
    "with strategy.scope():\n",
    "    model, history, scores = run(dict(metrics=metrics), dict(epochs=10, batch_size=4), dict())\n",
    "model.save('data/04_models/large', save_format='tf')\n",
    "print(scores)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  },
  "name": "transformer.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
